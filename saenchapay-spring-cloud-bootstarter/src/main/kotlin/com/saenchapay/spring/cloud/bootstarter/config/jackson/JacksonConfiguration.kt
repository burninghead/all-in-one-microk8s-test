package com.saenchapay.spring.cloud.bootstarter.config.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class JacksonConfiguration {

    @Bean
    open fun objectMapper(): ObjectMapper {
        val objectMapper = ObjectMapper()
        StaticJacksonProvider.objectMapper = objectMapper
        return objectMapper
    }

    object StaticJacksonProvider {

        var objectMapper: ObjectMapper? = null
            internal set

        fun toStringObject(value: Any): String = objectMapper?.writeValueAsString(value)!!

    }

}

