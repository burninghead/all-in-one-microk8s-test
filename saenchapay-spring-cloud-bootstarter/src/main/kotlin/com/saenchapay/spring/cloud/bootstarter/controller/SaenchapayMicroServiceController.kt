package com.saenchapay.spring.cloud.bootstarter.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class SaenchapayMicroServiceController(@param:Value("\${spring.application.name}") val applicationName: String) {

    @GetMapping("/common/microservice/name")
    fun microServiceName() = applicationName
}