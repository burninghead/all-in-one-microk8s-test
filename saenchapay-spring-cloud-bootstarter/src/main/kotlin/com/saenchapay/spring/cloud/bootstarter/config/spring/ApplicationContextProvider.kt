package com.saenchapay.spring.cloud.bootstarter.config.spring

import org.springframework.beans.BeansException
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.context.annotation.Configuration
import kotlin.jvm.Throws

@Configuration
open class ApplicationContextProvider : ApplicationContextAware {
    @Throws(BeansException::class)
    override fun setApplicationContext(applicationContext: ApplicationContext) {
        StaticApplicationContextProvider.setApplicationContext(applicationContext)
    }

    object StaticApplicationContextProvider {
        var applicationContext: ApplicationContext? = null
            private set

        internal fun setApplicationContext(applicationContext: ApplicationContext) {
            StaticApplicationContextProvider.applicationContext = applicationContext
        }
    }
}