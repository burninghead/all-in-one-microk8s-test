package com.saenchapay.spring.cloud.bootstarter.config.spring

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["com.saenchapay.spring.cloud.bootstarter.controller"])
open class ComponentScanConfiguration