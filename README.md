# Some micro services in Kubernets world

## Configuring a VM with Ubuntu Server and MicroK8S

### Download ubuntu server

https://releases.ubuntu.com/20.04.2/ubuntu-20.04.2-live-server-amd64.iso

### Install an Ubuntu Server : it already contains out of the box MicroK8s ( just need to click on the correct installation package )

`sudo apt-get install docker.io`

### Install GUI ( this below is Gnome, you can choose another one : https://phoenixnap.com/kb/how-to-install-a-gui-on-ubuntu, https://linoxide.com/linux-how-to/how-install-gui-ubuntu-server-guide )

`sudo apt-get install ubuntu-gnome-desktop`

### Restart and add VirtualBox add on

### Configure users

* add following bash function

`function caGroup(){ sudo groupadd $1; sudo usermod -aG $1 $USER; echo "Group $1 has been added to $USER";}`

* Add current user for Docker :
  `caGroup docker`

* Add current user for VirtualBox :
  `caGroup vboxsf`

* Add current user for MicroK8S :
  `caGroup microk8s
  sudo chown -f -R $USER ~/.kube`

### Reboot system

### Add alias 

```
bash
echo "alias kubectl='microk8s kubectl'" >> ~/.bash_aliases
bash
```

### Configure Microk8s add-ons

`microk8s enable dashboard dns ingress registry storage`

To have a look on all available : 

`microk8s status`

## Useful microk8s command

#### Pushed images in built in registry

http://127.0.0.1:32000/v2/_catalog

#### Open Kubernetes console

`microk8s dashboard-proxy`

## Micro services

### Architecture


                          ----------------
                          |              |
                          |              |
                          |   Mongo DB   |
                          |              |
                          |              |
                          ----------------
                          |              |
                          |              |
                  -------------       -------------
                  | vegetable |       |   fruit   |
                  |    shop   |       |    shop   |
                  -------------       -------------
                          |              |
                          |              |
                          |              |
                         ------------------
                         |   supermarket  |
                         ------------------

Only supermarket is exposed outside of the cluster on port 31000

### Deploy all Microservice

From the `kubernetes` folder, just execute

`deploy-all.bash`

Please note 

### Delete all Microservice

From the `kubernetes` folder, just execute

`delete-all.bash`

## Usefull command

Deploy a nginx server to execute command into kubernetes cluster

`kubectl run -it --rm --restart=Never nginx --image=nginx sh`













