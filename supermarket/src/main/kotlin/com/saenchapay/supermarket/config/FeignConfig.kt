package com.saenchapay.supermarket.config

import com.saenchapay.fruitshop.api.FruitResource
import com.saenchapay.vegetableshop.api.VegetableResource
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.cloud.openfeign.FeignClientsConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(value = [FeignClientsConfiguration::class])
@EnableFeignClients(basePackageClasses = [FruitResource::class, VegetableResource::class])
open class FeignConfig

