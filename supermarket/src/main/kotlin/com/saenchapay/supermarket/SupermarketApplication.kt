package com.saenchapay.supermarket


import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
@EnableDiscoveryClient
open class SupermarketApplication

fun main(args: Array<String>) {
    runApplication<SupermarketApplication>(*args)
}
