package com.saenchapay.supermarket.controller


import com.saenchapay.fruitshop.api.FruitResource
import com.saenchapay.vegetableshop.api.VegetableResource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/inventory/")
class
InventoryController(private val vegetableResource: VegetableResource,
                    private val fruitResource: FruitResource){


    @GetMapping("")
    fun helloWorld() = "hello my dear friend !"

    @GetMapping("allAvailable")
    fun getAllAvailable() = getAvailableFruit() + "\\n" + getAvailableVegetable()

    @GetMapping("fruitAvailable")
    fun getAvailableFruit() = fruitResource.getAvailableFruit()

    @GetMapping("vegetableAvailable")
    fun getAvailableVegetable() = vegetableResource.getAvailableVegetable()
}