package com.saenchapay.supermarket.controller

import com.saenchapay.fruitshop.api.FruitResource
import com.saenchapay.fruitshop.model.Fruit
import com.saenchapay.vegetableshop.api.VegetableResource
import com.saenchapay.vegetableshop.model.Vegetable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/marketplace/")
class MarketPlaceController(private val vegetableResource: VegetableResource,
                            private val fruitResource: FruitResource
){
    @GetMapping("buy/fruit/{fruit}/{quantity}")
    fun buyFruit(@PathVariable("fruit") fruit: Fruit,
                 @PathVariable("quantity") quantity : Int) : String {
        return fruitResource.buyFruit(fruit, quantity)
    }

    @GetMapping("sell/fruit/{fruit}/{quantity}")
    fun sellFruit(@PathVariable("fruit") fruit: Fruit,
                 @PathVariable("quantity") quantity : Int) : String {
        return fruitResource.sellFruit(fruit, quantity)
    }

    @GetMapping("buy/vegetable/{vegetable}/{quantity}")
    fun buyVegetable(@PathVariable("vegetable") vegetable: Vegetable,
                 @PathVariable("quantity") quantity : Int) : String {
        return vegetableResource.buyVegetable(vegetable, quantity)
    }

    @GetMapping("sell/vegetable/{vegetable}/{quantity}")
    fun sellVegetable(@PathVariable("vegetable") vegetable: Vegetable,
                  @PathVariable("quantity") quantity : Int) : String {
        return vegetableResource.sellVegetable(vegetable, quantity)
    }
}