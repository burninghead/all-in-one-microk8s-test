#!/bin/bash

cd $(dirname "$0")

DOCKER=/usr/bin/docker
KUBECTL="/snap/bin/microk8s kubectl"
MK8S_REPOSITORY="localhost:32000"

echo "Deploy FruitShop on Microk8s"

CONTAINER_ID=$(docker images | grep 'latest' | grep 'supermarket' | awk '{ print $3 }')

$DOCKER tag $CONTAINER_ID $MK8S_REPOSITORY/supermarket:registry
$DOCKER push $MK8S_REPOSITORY/supermarket:registry
