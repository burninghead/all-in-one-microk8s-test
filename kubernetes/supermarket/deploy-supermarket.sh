#!/bin/bash

cd $(dirname "$0")

KUBECTL="/snap/bin/microk8s kubectl"

echo "Deploy supermarket_deploy"
$KUBECTL apply -f supermarket_deploy.yaml
echo "Deploy supermarket_service"
$KUBECTL apply -f supermarket_service.yaml