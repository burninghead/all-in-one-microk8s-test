#!/bin/bash

KUBECTL_DELETE="/snap/bin/microk8s kubectl delete"

echo "Delete deployment supermarket"
$KUBECTL_DELETE deployment supermarket
echo ""
echo "Delete service supermarket"
$KUBECTL_DELETE service supermarket