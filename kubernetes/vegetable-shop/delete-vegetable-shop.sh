#!/bin/bash

KUBECTL_DELETE="/snap/bin/microk8s kubectl delete"

echo "Delete deployment vegetable-shop"
$KUBECTL_DELETE deployment vegetable-shop
echo ""
echo "Delete service vegetable-shop"
$KUBECTL_DELETE service vegetable-shop