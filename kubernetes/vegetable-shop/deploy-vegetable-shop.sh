#!/bin/bash

cd $(dirname "$0")

KUBECTL="/snap/bin/microk8s kubectl"

echo "Deploy vegetableshop_deploy"
$KUBECTL apply -f vegetableshop_deploy.yaml
echo "Deploy vegetableshop_service"
$KUBECTL apply -f vegetableshop_service.yaml