#!/bin/bash

cd $(dirname "$0")

DOCKER=/usr/bin/docker
KUBECTL="/snap/bin/microk8s kubectl"
MK8S_REPOSITORY="localhost:32000"

echo "Deploy VegetableShop on Microk8s"

CONTAINER_ID=$(docker images | grep 'latest' | grep 'vegetable-shop' | awk '{ print $3 }')

$DOCKER tag $CONTAINER_ID $MK8S_REPOSITORY/vegetable-shop:registry
$DOCKER push $MK8S_REPOSITORY/vegetable-shop:registry
