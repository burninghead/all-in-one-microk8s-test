#!/bin/bash

cd $(dirname "$0")
SCRIPT_PATH=`pwd`
declare -a dockerMicroservices=("vegetable-shop" "fruit-shop" "supermarket" "mongodb")

for ms in "${dockerMicroservices[@]}"
do
  echo "Delete K8S Object for $ms"
  DELETE_SCRIPT=$SCRIPT_PATH/$ms/delete-$ms.sh
  $DELETE_SCRIPT
done





