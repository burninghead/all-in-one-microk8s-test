#!/bin/bash

cd $(dirname "$0")

KUBECTL="/snap/bin/microk8s kubectl"

echo "Deploy fruitshop_deploy"
$KUBECTL apply -f fruitshop_deploy.yaml
echo "Deploy fruitshop_service"
$KUBECTL apply -f fruitshop_service.yaml