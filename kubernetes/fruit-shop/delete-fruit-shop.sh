#!/bin/bash

KUBECTL_DELETE="/snap/bin/microk8s kubectl delete"

echo "Delete deployment fruit-shop"
$KUBECTL_DELETE deployment fruit-shop
echo ""
echo "Delete service fruit-shop"
$KUBECTL_DELETE service fruit-shop