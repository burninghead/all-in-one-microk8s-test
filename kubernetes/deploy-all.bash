#!/bin/bash

cd $(dirname "$0")
source ./mongodb/mongodb_common.bash

declare -a dockerMicroservices=("vegetable-shop" "fruit-shop" "supermarket")

DOCKER=/usr/bin/docker
KUBECTL="/snap/bin/microk8s kubectl"
PAUSE="/usr/bin/sleep 5"
MK8S_REPOSITORY="localhost:32000"
GCR_REPOSITORY="gcr.io/saenchapay"


echo "Generate kubernetes files from template"

echo "Deploy MongoDB on Microk8s"

$DOCKER pull mongo:latest

echo "push latest mongo container in Microk8s repository"

CONTAINER_ID=$(docker images | grep 'latest' | grep 'mongo' | awk '{ print $3 }')

$DOCKER tag $CONTAINER_ID $MK8S_REPOSITORY/mongo:registry
$DOCKER push $MK8S_REPOSITORY/mongo

echo

if [ ! -d $LOCAL_MONGO_PATH ]
then
	echo "Need to create folder $LOCAL_MONGO_PATH for MongoDb database file"
	sudo mkdir -p $LOCAL_MONGO_PATH
fi

./mongodb/deploy-mongodb.sh




CPT=0
READY=$($KUBECTL get pod -l app=sp-mongodb -o jsonpath="{.items[0].status.containerStatuses[0].ready}")

while [ "$READY" != "true" ];
do
  CPT=$((CPT+1))
  if [ "$CPT" == "11" ];
  then
    echo "Max waiting retry attempt (10/10)...stop deployment"
    exit
  fi
  echo "Wait 5 seconds for POD Creation....attempt ($CPT/10)"
  $PAUSE
  READY=$($KUBECTL get pod -l app=sp-mongodb -o jsonpath="{.items[0].status.containerStatuses[0].ready}")
done

POD=$($KUBECTL get pod -l app=sp-mongodb -o jsonpath="{.items[0].metadata.name}")

$PAUSE

$KUBECTL exec $POD -- /usr/bin/mongo admin -u $MONGO_ROOT_NAME -p $MONGO_ROOT_PSWD --authenticationDatabase admin --eval "if(db.getUser(\"$MONGO_USER_NAME\")==null){ db.createUser({ user: \"$MONGO_USER_NAME\", pwd: \"$MONGO_USER_PSWD\", roles : [ { role : \"readWrite\" , db : \"saenchapay\"} ]});}"

cd $(dirname "$0")
SCRIPT_PATH=`pwd`

echo "Clean project and build docker images"

cd "$SCRIPT_PATH/.."
./gradlew clean jibDockerBuild -Pdebug

for ms in "${dockerMicroservices[@]}"
do

  echo ""

  IMAGE=$GCR_REPOSITORY/$ms

  CONTAINER_ID=$(docker images | grep 'latest' | grep $IMAGE | awk '{ print $3 }')
  $DOCKER tag $CONTAINER_ID $MK8S_REPOSITORY/$ms:registry
  $DOCKER push $MK8S_REPOSITORY/$ms
  DEPLOY_SCRIPT=$SCRIPT_PATH/$ms/deploy-$ms.sh
  $DEPLOY_SCRIPT
   
done
