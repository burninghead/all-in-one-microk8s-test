#!/bin/bash

cd $(dirname "$0")

KUBECTL="/snap/bin/microk8s kubectl"

echo "Deploy mongodb_secret"
$KUBECTL apply -f mongodb_secret.yaml

echo "Deploy mongodb_config"
$KUBECTL apply -f mongodb_config.yaml
echo "Deploy mongodb_deploy"
$KUBECTL apply -f mongodb_deploy.yaml
echo "Deploy mongodb_service"
$KUBECTL apply -f mongodb_service.yaml