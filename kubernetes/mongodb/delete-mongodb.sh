#!/bin/bash

KUBECTL_DELETE="/snap/bin/microk8s kubectl delete"

$KUBECTL_DELETE configMap sp-mongodb-config
$KUBECTL_DELETE secret sp-mongodb-secret
$KUBECTL_DELETE deployment sp-mongodb
$KUBECTL_DELETE service sp-mongodb