#!/bin/bash

cd $(dirname "$0")

DOCKER=/usr/bin/docker
KUBECTL="/snap/bin/microk8s kubectl"
MK8S_REPOSITORY="localhost:32000"

echo "Deploy MongoDB on Microk8s"

$DOCKER pull mongo:latest

echo "push latest mongo container in Microk8s repository"

CONTAINER_ID=$(docker images | grep 'latest' | grep 'mongo' | awk '{ print $3 }')

$DOCKER tag $CONTAINER_ID $MK8S_REPOSITORY/mongo:registry
$DOCKER push $MK8S_REPOSITORY/mongo:registry
