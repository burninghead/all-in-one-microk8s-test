#!/bin/bash

PAUSE="/usr/bin/sleep 5"
DOCKER=/usr/bin/docker

$DOCKER rmi -f $($DOCKER images)
MICROK8S=/snap/bin/microk8s

$MICROK8S disable registry
echo ""
$MICROK8S disable storage:destroy-storage
echo ""
$PAUSE
$MICROK8S enable storage
echo ""
$MICROK8S enable registry


