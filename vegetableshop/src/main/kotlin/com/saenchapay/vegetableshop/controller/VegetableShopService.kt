package com.saenchapay.vegetableshop.controller



import com.saenchapay.vegetableshop.api.VegetableResource
import com.saenchapay.vegetableshop.model.Vegetable
import com.saenchapay.vegetableshop.model.VegetableStore
import com.saenchapay.vegetableshop.repository.VegetableRepository
import org.springframework.web.bind.annotation.RestController

@RestController
class VegetableShopService(
    private val vegetableRepository: VegetableRepository,
) : VegetableResource {
    override fun getAvailableVegetable(): List<Vegetable> {
        return vegetableRepository.findAll().map { vegetableStore -> vegetableStore.vegetable }.toList()
    }


    override fun buyVegetable(vegetable: Vegetable, quantity: Int): String {
        var availableVegetable = vegetableRepository.findById(vegetable.toString()).orElse(VegetableStore(vegetable,0));
        if(availableVegetable.quantity == 0)return "There is no {fruit} available"

        if(quantity>availableVegetable.quantity){
            vegetableRepository.save(VegetableStore(vegetable,0));
            val boughtVegetable = quantity - availableVegetable.quantity
            return "Not enough $vegetable available, you only buy $boughtVegetable"
        }

        val remainingQuantity = availableVegetable.quantity - quantity

        vegetableRepository.save(VegetableStore(vegetable,remainingQuantity));

        return "You just buy $quantity $vegetable, $remainingQuantity left"
    }

    override fun sellVegetable(vegetable: Vegetable, quantity: Int): String {
        val availableVegetable = vegetableRepository.findById(vegetable.toString()).orElse(VegetableStore(vegetable,0));
        val newQuantity = availableVegetable.quantity + quantity
        vegetableRepository.save(VegetableStore(vegetable,newQuantity))
        return "There is now $newQuantity $vegetable(s)"
    }

}