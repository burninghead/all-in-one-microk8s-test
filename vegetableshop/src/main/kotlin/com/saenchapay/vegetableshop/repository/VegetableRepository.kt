package com.saenchapay.vegetableshop.repository

import com.saenchapay.vegetableshop.model.VegetableStore

import org.springframework.data.mongodb.repository.MongoRepository

interface VegetableRepository : MongoRepository<VegetableStore, String>{
    fun findVegetableStoreByQuantityGreaterThan(quantity : Int)
}