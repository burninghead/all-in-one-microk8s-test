package com.saenchapay.vegetableshop

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class VegetableShopApplication

fun main(args: Array<String>) {
    runApplication<VegetableShopApplication>(*args)
}
