package com.saenchapay.vegetableshop.model

import com.saenchapay.spring.cloud.bootstarter.config.jackson.JacksonConfiguration.StaticJacksonProvider.toStringObject
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("vegetable-store")
class VegetableStore(
    @Id val vegetable: Vegetable,
    var quantity: Int,

    ) {
    override fun toString(): String = toStringObject(this)
}
