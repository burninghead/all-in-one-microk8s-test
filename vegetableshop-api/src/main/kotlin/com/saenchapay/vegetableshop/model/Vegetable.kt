package com.saenchapay.vegetableshop.model

enum class Vegetable {
    Potato,Peas,Lettuce,Tomatoes,Cucumber,Sweetcorn
}

