package com.saenchapay.vegetableshop.api

import com.saenchapay.vegetableshop.model.Vegetable
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.*

@FeignClient(value = "vegetable-shop")
@RequestMapping("/vegetable-shop/")
interface VegetableResource {

    @GetMapping("/available")
    fun getAvailableVegetable(): List<Vegetable>

    @GetMapping("buy/{vegetable}/{quantity}")
    fun buyVegetable(
        @PathVariable("vegetable") vegetable: Vegetable,
        @PathVariable("quantity") quantity : Int,
    ): String

    @GetMapping("sell/{vegetable}/{quantity}")
    fun sellVegetable(
        @PathVariable("vegetable") vegetable: Vegetable,
        @PathVariable("quantity") quantity : Int,
    ): String

}