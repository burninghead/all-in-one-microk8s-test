package com.saenchapay.fruitshop

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class FruitshopApplication

fun main(args: Array<String>) {
    runApplication<FruitshopApplication>(*args)
}
