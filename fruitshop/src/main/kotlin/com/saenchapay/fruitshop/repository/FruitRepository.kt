package com.saenchapay.fruitshop.repository

import com.saenchapay.fruitshop.model.FruitStore
import org.springframework.data.mongodb.repository.MongoRepository

interface FruitRepository : MongoRepository<FruitStore, String>{
    fun findFruitStoreByQuantityGreaterThan(quantity : Int)
}