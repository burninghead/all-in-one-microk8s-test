package com.saenchapay.fruitshop.controller


import com.saenchapay.fruitshop.api.FruitResource
import com.saenchapay.fruitshop.model.Fruit
import com.saenchapay.fruitshop.model.FruitStore
import com.saenchapay.fruitshop.repository.FruitRepository
import org.springframework.web.bind.annotation.RestController

@RestController
class FruitshopService(
    private val fruitRepository: FruitRepository,
) : FruitResource {

    override fun getAvailableFruit(): List<Fruit> {
        return fruitRepository.findAll().map { fruitStore -> fruitStore.fruit }.toList()
    }

    override fun buyFruit(fruit: Fruit, quantity: Int): String {
        var availableFruit = fruitRepository.findById(fruit.toString()).orElse(FruitStore(fruit,0));
        if(availableFruit.quantity == 0)return "There is no {fruit} available"

        if(quantity>availableFruit.quantity){
            fruitRepository.save(FruitStore(fruit,0));
            val boughtFruit = quantity - availableFruit.quantity
            return "Not enough $fruit available, you only buy $boughtFruit"
        }

        val remainingQuantity = availableFruit.quantity - quantity

        fruitRepository.save(FruitStore(fruit,remainingQuantity));

        return "You just buy $quantity $fruit, $remainingQuantity left"
    }

    override fun sellFruit(fruit: Fruit, quantity: Int): String {
        val availableFruit = fruitRepository.findById(fruit.toString()).orElse(FruitStore(fruit,0));
        val newQuantity = availableFruit.quantity + quantity
        fruitRepository.save(FruitStore(fruit,newQuantity))
        return "There is now $newQuantity $fruit(s)"

    }

}