package com.saenchapay.fruitshop.api

import com.saenchapay.fruitshop.model.Fruit
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.*

@FeignClient(value = "fruit-shop")
@RequestMapping("/fruit-shop/")
interface FruitResource {

    @GetMapping("/available")
    fun getAvailableFruit(): List<Fruit>

    @GetMapping("buy/{fruit}/{quantity}")
    fun buyFruit(
        @PathVariable("fruit") fruit: Fruit,
        @PathVariable("quantity") quantity : Int,
    ): String

    @GetMapping("sell/{fruit}/{quantity}")
    fun sellFruit(
        @PathVariable("fruit") fruit: Fruit,
        @PathVariable("quantity") quantity : Int,
    ): String

}