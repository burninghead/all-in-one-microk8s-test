package com.saenchapay.fruitshop.model

import com.saenchapay.spring.cloud.bootstarter.config.jackson.JacksonConfiguration.StaticJacksonProvider.toStringObject
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("fruit-store")
class FruitStore(
    @Id val fruit: Fruit,
    var quantity: Int,

    ) {
    override fun toString(): String = toStringObject(this)
}
