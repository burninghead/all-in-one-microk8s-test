package com.saenchapay.fruitshop.model

enum class Fruit {
    Banana,Orange,Lemon,Pineapple,Apple,Watermelon
}